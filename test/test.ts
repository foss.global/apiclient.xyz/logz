import { expect, tap } from '@pushrocks/tapbundle';
import * as logz from '../ts/index';
import { Qenv } from '@pushrocks/qenv';

import { ILogPackage } from '@pushrocks/smartlog-interfaces';

const testQenv = new Qenv('./', './.nogit');

tap.test('should sne dlog lines to logz', async () => {
  const logzInstance = new logz.Logz(testQenv.getEnvVarOnDemand('LOGZ_TOKEN'));
  const testLogPackage: ILogPackage  = {
    context: {
      company: 'Lossless GmbH',
      companyunit: 'Lossless Cloud',
      containerName: 'shipzone_mojoio_logz',
      environment: 'test',
      runtime: 'node',
      zone: 'gitzone'
    },
    level: 'info',
    message: 'this is a test message send from the logz.io package',
    timestamp: Date.now(),
    type: 'log'
  }
  logzInstance.sendLogLines([
    testLogPackage
  ]);
  logzInstance.smartlogDestination.handleLog(testLogPackage);
  logzInstance.smartlogDestination.handleLog(testLogPackage);
  logzInstance.smartlogDestination.handleLog(testLogPackage);
});

tap.start();
