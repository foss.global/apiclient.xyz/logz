import * as plugins from './logz.plugins';

/**
 * the main logz API package
 */
export class Logz {
  private shippingToken: string;
  constructor(tokenArg: string) {
    this.shippingToken = tokenArg;
  }

  /**
   * sends log lines towards logz.io
   * @param logLineArrayArg
   */
  public async sendLogLines(logLineArrayArg: plugins.smartlogInterfaces.ILogPackage[]) {
    const url = `https://listener-nl.logz.io:8071/?token=${this.shippingToken}&type=SMARTLOG`;
    let postBody = ``;
    for (const logLine of logLineArrayArg) {
      postBody += JSON.stringify(logLine) + '\n';
    }
    console.log(postBody);
    const response = await plugins.smartrequest.request(url, {
      method: 'POST',
      requestBody: postBody
    });
    console.log(response.body);
  }

  public logAggregator = new plugins.lik.TimedAggregtor<plugins.smartlogInterfaces.ILogPackage>({
    aggregationIntervalInMillis: 1000,
    functionForAggregation: (aggregation => {
      this.sendLogLines(aggregation);
    })
  });
  public get smartlogDestination(): plugins.smartlogInterfaces.ILogDestination {
    return {
      handleLog: logPackage => {
        this.logAggregator.add(logPackage);
      }
    };
  }
}
